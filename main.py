import os
import base64

import flask

import hidden_buffer_query_log
import verify_quote
from public_key import pk

app = flask.Flask(__name__)

nonces = set()
def new_nonce():
    nonce = os.urandom(100)
    nonces.add(nonce)
    return nonce

def check_nonce(n):
    """will erase if present"""
    if n in nonces:
        nonces.remove(n)
        return True
    else:
        return False

@app.route("/login", methods=["POST"])
def submit():
    flask.request.get_data()

    nonce = base64.b64decode(flask.request.form['nonce'])

    app_id = flask.request.headers['X-APP-ID']

    email = flask.request.form['email']
    email_log = flask.request.form['email-query-log']

    password = flask.request.form['password']
    password_log = flask.request.form['password-query-log']

    sig = base64.b64decode(flask.request.headers['x-attestation-signature'])

    sig_verify = verify_quote.verify(flask.request.data, nonce, sig, pk)

    print "sig verify: %r" % sig_verify
    print "nonce match: %r" % check_nonce(nonce)
    print "intended URL: %r" % flask.request.form['exfiltration-url']
    print "email: %s" % email
    print "email log: %r" % hidden_buffer_query_log.parse(base64.b64decode(email_log))
    print "password: %s" % password
    print "password_log: %r" % hidden_buffer_query_log.parse(base64.b64decode(password_log))

    if sig_verify:
        return "%s HURRAY" % app_id
    else:
        return "%s is cheater" % app_id

@app.route("/httpapp", methods=["POST"])
def httpapp_submit():
    return "Nice job, HTTPAPP!"

@app.route("/httpapp_2/nonce")
@app.route("/httpapp_/nonce")
@app.route("/login/nonce")
def nonce():
    return new_nonce()


if __name__ == "__main__":
    app.debug = True
    app.run('0.0.0.0', port=int(os.environ["PORT"]))
