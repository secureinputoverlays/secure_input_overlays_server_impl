"""
Sanity check for TPMSign implementation

by reimplementing it in python without looking :/
"""
import struct
import hashlib
import base64

import rsa

def verify(data, nonce, signature, pk):
    dataDigest = hashlib.sha1(data).digest()
    nonceDigest = hashlib.sha1(nonce).digest()

    # Pcr Selection - just 23
    pcr_selection = struct.pack(">H B B B", 3, 0x00, 0x00, 0x80)

    # Extend dataDigest into pcr23
    pcr23 = hashlib.sha1('\x00'*20 + dataDigest).digest()

    pcr_composite = struct.pack(">5s I 20s", pcr_selection, 20, pcr23)
    composite_hash = hashlib.sha1(pcr_composite).digest()

    version = 0x01010000
    fixed = 'QUOT'
    quote_info = struct.pack(">I 4s 20s 20s", version, fixed, composite_hash, nonceDigest)

    try:
        rsa.verify(quote_info, signature, pk)
    except rsa.VerificationError:
        return False

    return True
