"""
Parses query output
"""
import struct
import base64

LENGTH_QUERY = 0
REGEX_QUERY = 1
EBPF_QUERY = 2

JAVA_REGEX_FLAGS = [
    # java name, java int, python name (can get int automagically)
    ('UNIX_LINES',              1,      None),
    ('CASE_INSENSITIVE',        2,      'IGNORECASE'),
    ('COMMENTS',                4,      'VERBOSE'),
    ('MULTILINE',               8,      'MULTILINE'),
    ('LITERAL',                 16,     None),
    ('DOTALL',                  32,     'DOTALL'),
    ('UNICODE_CASE',            64,     None),
    ('CANON_EQ',                128,    None),
    ('UNICODE_CHARACTER_CLASS', 256,    None),
]

def stringify_regex_flags(i):
    flags = []
    for java_name, java_int, python_name in JAVA_REGEX_FLAGS:
        if i & java_int:
            flags.append(java_name)
    if flags:
        return "(" + '|'.join(flags) + ")"
    else:
        return ""

class HBQuery(object):

    def __init__(self, query_type, data):
        self.query_type = query_type
        self.data = data

        if (query_type == LENGTH_QUERY):
            if (self.data):
                raise ValueError("LENGTH_QUERY should have no data")

        if (query_type == REGEX_QUERY):
            if (len(self.data) < 4):
                raise ValueError("REGEX_QUERY must have at least four bytes for flags")
            (flags,) = struct.unpack(">I", data[:4])
            self.s_flags = stringify_regex_flags(flags)
            

    def __repr__(self):
        if self.query_type == LENGTH_QUERY:
            return "[LENGTH]"
        elif self.query_type == REGEX_QUERY:
            return ("[REGEX%s: " % self.s_flags) + self.data[3:].encode('utf8') + "]"
        elif self.query_type == EBPF_QUERY:
            return "[EBPF: " + base64.b64encode(self.data) + "]"
        else:
            raise ValueError("Unknown query type")

valid_query_types = range(0, 3)

def parse(query_log):
    if not query_log:
        return []

    rest, out = parse_one(query_log)
    return [out] + parse(rest)

def parse_one(query_log):
    if len(query_log) < 5:
        raise ValueError("Not enough bytes for header")

    header = query_log[:5]
    rest = query_log[5:]
    query_type, length = struct.unpack(">bI", header)
    if query_type not in valid_query_types:
        raise ValueError("Query type %d not valid" % query_type)

    if len(rest) < length:
        raise ValueError("Specified length is %d, but only %d remaining" % (length, len(rest)))

    data = rest[:length]
    rest = rest[length:]
    return rest, HBQuery(query_type, data)


# Small test?
if __name__ == "__main__":
    s = "\x00\x00\x00\x00\x00" + "\x01\x00\x00\x00\x03\\d+"
    print parse(s)
